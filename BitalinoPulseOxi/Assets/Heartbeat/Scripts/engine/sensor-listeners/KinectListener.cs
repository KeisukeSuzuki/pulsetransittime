using UnityEngine;
using System.Collections;
using yarp_cs_wrapper;

using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.IO;
using System;

public delegate void KinectEventHandler( Bottle bottle );

public class KinectListener : MonoBehaviour
{

	private BufferedPortBottle rightHandMovPort;
	private BufferedPortBottle kinectGesturesPort;
	
	private System.Threading.Thread rightHandMovThread;
	private System.Threading.Thread kinectGesturesThread;	
	
	private bool run = true;
	public event KinectEventHandler kinectEventRightHandMov;
	public event KinectEventHandler kinectGestureEvent;	
	
	void Start(){
		
		Debug.Log("start kinect listener");
		
		try{
			Network.init();
			
			rightHandMovPort = new BufferedPortBottle();
	        bool status = rightHandMovPort.open("/ce/kinect/finger/in");
			Network.connect("/ssi/kinect/finger/out","/ce/kinect/finger/in");
			
			rightHandMovThread = new System.Threading.Thread(new System.Threading.ThreadStart( listenrightHandMovThread ) );		
			rightHandMovThread.Name = "TrazaReceiver YARP listen thread";
			rightHandMovThread.Start();
			
			kinectGesturesPort = new BufferedPortBottle();
	        status = kinectGesturesPort.open("/ce/kinect/gestures/in");
			Network.connect("/ssi/kinect/gestures/out","/ce/kinect/gestures/in");
			
			kinectGesturesThread = new System.Threading.Thread(new System.Threading.ThreadStart( listenKinectGesturesThread ) );		
			kinectGesturesThread.Name = "TrazaReceiver YARP listen thread";
			kinectGesturesThread.Start();
				
		}catch(Exception e){
			Debug.Log(e.Message);
		}

	}	
	
	void OnDestroy(){
		run = false;
		if (rightHandMovThread != null) rightHandMovThread.Abort();
		if (kinectGesturesThread != null) kinectGesturesThread.Abort();
		
		
		if(rightHandMovPort != null){
			rightHandMovPort.close();
		}
		if(kinectGesturesPort != null){
			kinectGesturesPort.close();
		}
		Network.fini();
		
	}
	
    private void listenrightHandMovThread(){
		
		Debug.Log("listenrightHandMovThread");
		while(run){
			
			try{
				//Read the bottle
				
		        Bottle bottleIn = rightHandMovPort.read(true); 
								
				if(bottleIn!=null){
					if(kinectEventRightHandMov != null){
						kinectEventRightHandMov(bottleIn);
					}					
				}
			}
			catch (Exception e)
	        {
	           Debug.Log( e.ToString() );
	        }

			//System.Threading.Thread.Sleep( 500 ); // ms
		}
	}

    private void listenKinectGesturesThread(){
		
		Debug.Log("listenKinectGesturesThread");
		while(run){
			
			try{
				//Read the bottle
				
		        Bottle bottleIn = kinectGesturesPort.read(true); 
								
				if(bottleIn!=null){
					Debug.Log(bottleIn.toString());
					//Depending on the message create an event
					if(kinectGestureEvent != null){
						kinectGestureEvent(bottleIn);
					}					
				}
			}
			catch (Exception e)
	        {
	           Debug.Log( e.ToString() );
	        }

			//System.Threading.Thread.Sleep( 500 ); // ms
		}
	}
		
}
