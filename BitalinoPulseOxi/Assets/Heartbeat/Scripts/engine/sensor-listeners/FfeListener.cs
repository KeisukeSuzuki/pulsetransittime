using UnityEngine;
using System.Collections;
using yarp_cs_wrapper;

using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.IO;
using System;

public delegate void FfeEventHandler( Bottle bottle );

public class FfeListener : MonoBehaviour
{

	private BufferedPortBottle inPort;
	private System.Threading.Thread lThread;
	private bool run = true;
	public event FfeEventHandler ffeEvent;
	
	void Start(){
	
		Network.init();
		inPort = new BufferedPortBottle();
        bool status = inPort.open("/sensor-test/ffe/in");
		Network.connect("/ssi/ffe/shore_sel/out","/sensor-test/ffe/in");
		
		Debug.Log ("Opened YARP port: "+status);
		
		lThread = new System.Threading.Thread(new System.Threading.ThreadStart( listenYarpThread ) );		
		lThread.Name = "TrazaReceiver YARP listen thread";
		lThread.Start();
		
		
	}
	
    void Update(){
		
    }	
	
	void OnDestroy(){
		run = false;
		if (lThread != null) lThread.Abort();
		if(inPort!=null){
			inPort.close();
		}
        Network.fini();
		
	}
	
    private void listenYarpThread(){
		
		while(run){
			try{
				//Read the bottle
		        Bottle bottleIn = inPort.read(false); 
			
				if(bottleIn!=null){
					//Debug.Log(bottleIn.toString());
					if(ffeEvent!=null){
						//Debug.Log(bottleIn.toString());
						ffeEvent(bottleIn);
					}else{
						//Debug.Log("ffeEvent = null");
					}
				}
			}
			catch (Exception e)
	        {
	           Debug.Log( e.ToString() );
	        }
			//System.Threading.Thread.Sleep( 500 ); // ms
		}
	}
	
}
