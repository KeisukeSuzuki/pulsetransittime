using UnityEngine;
using System;
using System.IO;
using System.Diagnostics;


public class StopWatchTimer : MonoBehaviour {

	
	public Stopwatch stopWatch = new Stopwatch();
	public bool isStart = false;

	void Start()
	{
	}
		
	public void StartWatch()
	{

		stopWatch.Start ();
		isStart = true;
	}


	public int GetElapsedMilliseconds()
	{
		return (int)stopWatch.ElapsedMilliseconds;
	}

	public string getTime()
	{
		return stopWatch.Elapsed.TotalSeconds.ToString();
	}



}

