﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Heartbeat
{
	public enum FeedbackType
	{
		Raw, Sync, Delay, Slow, Fast, Same, Systole, Diastole
	}
}
