using UnityEngine;
using System.Collections;
using 	yarp_cs_wrapper;

using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.IO;
using System;

public delegate void CSAEventHandler( Bottle bottle );

/*
 *
 */
public class CSAReceiver : MonoBehaviour {
	
	private BufferedPortBottle inPort;
	private System.Threading.Thread lThread;
	private bool run = true;
	public event CSAEventHandler csaEvent; //narrative generator event

	void Awake(){
		DontDestroyOnLoad(gameObject);
	}
	
	void Start(){
			
		Network.init();
		inPort = new BufferedPortBottle();
        bool status = inPort.open("/unity/ce/csa/in");
		Debug.Log("CSAReceiver " + status);
		Network.connect("/csa/state/out","/unity/ce/csa/in");
		
		lThread = new System.Threading.Thread(new System.Threading.ThreadStart( listenYarpThread ) );		
		lThread.Name = "CSAReceiver YARP listen thread";
		lThread.Start();
	}	
	
	void OnApplicationQuit (){
		run = false;
		if (lThread != null) lThread.Abort();
		if(inPort != null){
			inPort.close();
		}
        Network.fini();
		
	}
	
    private void listenYarpThread(){
		
		while(run){
			try{
				//Read the bottle
				
		        Bottle bottleIn = inPort.read(true); 
								
				if(bottleIn!=null){
					if(csaEvent != null){
						csaEvent(bottleIn);
					}					
				}
			}
			catch (Exception e)
	        {
	         	Debug.Log(e.ToString()); 
	        }
		}
	}

}
