close all
clear all

addpath('./ecgpeak');

%% read

results = [];
results = [results, calcTransitTime('ksk', 'pulseoxi-2015-4-20-17-36-30.csv', 'bitalino_0420173630606_Data.csv',    true, 10, 150)];
results = [results, calcTransitTime('acer', 'pulseoxi-2015-4-21-16-14-38.csv', 'bitalino_0421161438120_Data.csv',   false, 65, 105)];
results = [results, calcTransitTime('g', 'pulseoxi-2015-4-21-17-10-13.csv', 'bitalino_0421171013420_Data.csv',      true, 10, 60)];
results = [results, calcTransitTime('george', 'pulseoxi-2015-4-21-17-23-27.csv', 'bitalino_0421172327845_Data.csv', true, 10, 80)];
results = [results, calcTransitTime('hielke', 'pulseoxi-2015-4-21-17-31-2.csv', 'bitalino_0421173102346_Data.csv',  true, 10, 40)];



results.sts_mean

results.sts_std