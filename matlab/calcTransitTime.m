function [ result ] = calcTransitTime( directory, oxi_filename, bit_filename, inverted, start_time, end_time )


oxi_data = csvread( [directory, '/', oxi_filename] );
bit_data = csvread( [directory, '/', bit_filename] , 1,0);

oxi_data = oxi_data(find(oxi_data(:,1) > (start_time * 1000) & oxi_data(:,1) < (end_time * 1000) ), :); 
bit_data = bit_data(find(bit_data(:,1) > (start_time * 1000) & bit_data(:,1) < (end_time * 1000) ), :); 

if inverted    
   bit_data(:,2) = -bit_data(:,2);
end

result = func_peakdetect(bit_data(:,2)', 100, false);

%% figure
figure
subplot(311)
plot(oxi_data(:,1), oxi_data(:,2)-10000, 'r');
hold on 
stem(oxi_data(:,1), oxi_data(:,3)*50000, ':g' );
stem(oxi_data(:,1), oxi_data(:,4)*40000, '-b' );

subplot(312)
plot(bit_data(:,1), bit_data(:,2)*10000, 'r');
hold on
stem(bit_data(:,1), bit_data(:,3)*10000, ':g');

stem(bit_data(:,1), result.peaks2*10000, '-b');
    
peak_data = result.peaks2;
% peak_data = bit_data(:,3);

peak_times = find(peak_data == 1);

%% pulse transit time
pulses = [];
for i=1:length(peak_times)-1

   
    
    ecg_time = bit_data(peak_times(i), 1);
    next_ecg_time = bit_data(peak_times(i+1), 1);       
    
    sucess_raw = false;
    oxi_raw_idx = find(ecg_time < oxi_data(:,1) & oxi_data(:,3) == 1);
    if (length(oxi_raw_idx) > 0)
        oxi_raw_time = oxi_data(oxi_raw_idx(1), 1);
        if ( oxi_raw_time - ecg_time < 500 ) & ( oxi_raw_time < next_ecg_time)
            sucess_raw = true;
        end
    end
    sucess_sts = false;
    oxi_sts_idx = find(ecg_time < oxi_data(:,1) & oxi_data(:,4) == 1);
    if (length(oxi_sts_idx) > 0)
        oxi_sts_time = oxi_data(oxi_sts_idx(1), 1);
        if ( oxi_sts_time - ecg_time < 600)  & ( oxi_sts_time < next_ecg_time)
            sucess_sts = true;
        end
    end
    if (sucess_raw & sucess_sts)
        pulses = [ pulses; ecg_time, oxi_raw_time, oxi_sts_time];
    end
   
end


subplot(313)
for i=1:length(pulses)
   if rem(i,3) == 0
       colors = {'b', 'g'}; range = [0, 1];
   elseif rem(i,3) == 1
       colors = {'r', 'k'}; range = [1, 2];
   else
       colors = {'c', 'm'}; range = [2, 3];
   end

    plot( [pulses(i, 1),pulses(i,1)], range, colors{1} );
    hold on
    plot( [pulses(i, 3),pulses(i,3)], range, colors{2});
end


sub_raw = pulses(:,2) - pulses(:,1);
sub_sts = pulses(:,3) - pulses(:,1);
result.raw_mean = mean(sub_raw);
result.raw_std = std(sub_raw);

result.sts_mean = mean(sub_sts);
result.sts_std = std(sub_sts);


sub_raw_sts = pulses(:,3) - pulses(:,2);
result.raw_minus_sts_mean = mean(sub_raw_sts);
result.raw_minus_sts_std = std(sub_raw_sts);


end

