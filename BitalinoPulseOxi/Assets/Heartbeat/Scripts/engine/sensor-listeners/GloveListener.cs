using UnityEngine;
using System.Collections;
using yarp_cs_wrapper;

using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.IO;
using System;

public delegate void GloveEventGrabHandler( Bottle bottle );
public delegate void GloveEventRotHandler( Bottle bottle );
public delegate void GloveEventGsrHandler( Bottle bottle );

public class GloveListener : MonoBehaviour
{

	private BufferedPortBottle inPortGrab;
	private BufferedPortBottle inPortRot;
	private BufferedPortBottle inPortGsr;
	private System.Threading.Thread lThread;
	private bool run = true;
	public event GloveEventGrabHandler gloveEventGrab;
	public event GloveEventRotHandler gloveEventRot;
	public event GloveEventGsrHandler gloveEventGsr;
	
	
	void Start(){
		
		Network.init();
		bool status;

		//
		inPortGrab = new BufferedPortBottle();
        status = inPortGrab.open("/sensor-test/glove/grab/in");
		Network.connect("/ssi/glove/grab/out","/sensor-test/glove/grab/in");
		Debug.Log ("Opened YARP port grab: "+status);
		
		//
		inPortGsr = new BufferedPortBottle();
        status = inPortGsr.open("/sensor-test/glove/gsr/in");
		Network.connect("/ssi/glove/gsr/out","/sensor-test/glove/gsr/in");	
		Debug.Log ("Opened YARP port gsr: "+status);

		inPortRot = new BufferedPortBottle();
        status = inPortRot.open("/sensor-test/glove/rot/in");
		Network.connect("/ssi/glove/rot/out","/sensor-test/glove/rot/in");	
		Debug.Log ("Opened YARP port rot: "+status);

		lThread = new System.Threading.Thread(new System.Threading.ThreadStart( listenYarpThread ) );		
		lThread.Name = "TrazaReceiver YARP listen thread";
		lThread.Start();
	}	
	
	void OnDestroy(){
		run = false;
		if (lThread != null) lThread.Abort();
		if(inPortGrab!=null) inPortGrab.close();
		if(inPortRot!=null) inPortRot.close();
		if(inPortGsr!=null) inPortGsr.close();
		Network.fini();
		
	}
	
    private void listenYarpThread(){
		
		while(run){
			
			try{
				//Read the bottle
	
		        Bottle bottleInGsr = inPortGsr.read(false); 
								
				if(bottleInGsr!=null){
					//Debug.Log("Arousal: "+bottleInGsr.toString());
					//Depending on teh message create an event
					if(gloveEventGsr != null){
						gloveEventGsr(bottleInGsr);
					}					
				}
				
		        Bottle bottleInGrab = inPortGrab.read(false); 
								
				if(bottleInGrab!=null){
					//Debug.Log(bottleInGrab.toString());
					//Depending on teh message create an event
					if(gloveEventGrab != null){
						gloveEventGrab(bottleInGrab);
					}		
					
				}
		
		        Bottle bottleInRot = inPortRot.read(false); 
								
				if(bottleInRot!=null){
					//Debug.Log(bottleInRot.toString());
					//Depending on teh message create an event
					if(gloveEventRot != null){
						gloveEventRot(bottleInRot);
					}					
				}
			}
			catch (Exception e)
	        {
	           Debug.Log( e.ToString() );
	        }

			//System.Threading.Thread.Sleep( 500 ); // ms
		}
	}
	
}
