using UnityEngine;
using System.Collections;
using yarp_cs_wrapper;

using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.IO;
using System;

public delegate void OnTrackingMsg( Bottle bottle );

/*
 *
 */
public class TrackingReceiver : MonoBehaviour {
	
	private BufferedPortBottle inPort;
	private System.Threading.Thread lThread;
	private bool run = true;
	public event OnTrackingMsg trackingMsg;

	void Awake(){
		DontDestroyOnLoad(gameObject);
	}
	
	void Start(){
		
		Network.init();
		inPort = new BufferedPortBottle();
        bool status = inPort.open("/csa/tracking/in");
		Debug.Log("Tracking Receiver " + status);
		Network.connect("/Traza/Out/Positions","/csa/tracking/in");
		
		lThread = new System.Threading.Thread(new System.Threading.ThreadStart( listenYarpThread ) );		
		lThread.Name = "Tracking Receiver YARP listen thread";
		lThread.Start();
	}	
	
	void OnApplicationQuit (){
		run = false;
		if (lThread != null) lThread.Abort();
		if(inPort != null){
			inPort.close();
		}
        Network.fini();
	}
	
    private void listenYarpThread(){
		
		while(run){
			try{
				//Read the bottle
		        Bottle bottleIn = inPort.read(true); 
								
				if(bottleIn!=null){

					if(trackingMsg != null){
						trackingMsg(bottleIn);
					}					
				}
			}
			catch (Exception e)
	        {
	         	Debug.Log(e.ToString()); 
	        }
		}
	}

}
