using UnityEngine;
using System.Collections;
using yarp_cs_wrapper;

using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.IO;
using System; 


using System.Diagnostics; 

public delegate void SmartexEventHandler(Bottle bottle);

public class SmartexListener : MonoBehaviour
{

	private BufferedPortBottle inPort;
	private System.Threading.Thread lThread;
	private bool run = true;
	public event SmartexEventHandler SmartextEvent;
	
	//debug
	/*
	Stopwatch sw = new Stopwatch();
	double pretime = 0;
	double pretime1sec = 0;
	int debug_count =0;
	double max_diff = 0;
	*/
	
	void Start(){
		UnityEngine.Debug.Log("Start");
		
		Network.init();
		inPort = new BufferedPortBottle();
        bool status = inPort.open("/ce/smartex/in");
		//Network.connect("/ssi/smartex/hrv/out","/ce/smartex/in");
		Network.connect("/ssi/smartex/ecg/out","/ce/smartex/in");
		
		UnityEngine.Debug.Log ("Opened YARP port: "+status);
		
		lThread = new System.Threading.Thread(new System.Threading.ThreadStart( listenYarpThread ) );		
		lThread.Name = "TrazaReceiver YARP listen thread";
		lThread.Start();
		
		//debug
		//sw.Start ();
	}
	
    void Update(){
		
    }	
	
	void OnDestroy(){
		run = false;
		if (lThread != null) lThread.Abort();
		inPort.close();
        Network.fini();
		
	}
	
    private void listenYarpThread(){
		
		while(run){
			try{
				//Read the bottle
		        Bottle bottleIn = inPort.read(false); 
			
				if(bottleIn!=null){
					//Debug.Log(bottleIn.toString());
					if(SmartextEvent!=null){
						//debug
						/*
						double diff = sw.ElapsedMilliseconds - pretime;
						debug_count++;			
						pretime = sw.ElapsedMilliseconds;
		
						if(sw.ElapsedMilliseconds - pretime1sec > 1000)
						{
							if(diff > max_diff)
							{
								max_diff = diff;
							}
							//UnityEngine.Debug.Log (" count:" + debug_count + " max msec:" + max_diff);	
						
							if(max_diff > 100){
								//UnityEngine.Debug.Log (" 100msc over - count:" + debug_count + " max msec:" + max_diff);	
							}
							max_diff = 0;
							debug_count = 0;
							pretime1sec = sw.ElapsedMilliseconds;
						}
						*/
						//debug end
						
						SmartextEvent(bottleIn);
					}
				}
			}
			catch (Exception e)
	        {
	           UnityEngine.Debug.Log( e.ToString() );
	        }
		}
	}
	
}
